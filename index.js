console.log(`Hello World!`);

function checkNumbers(num1, num2) {

    let num1New = parseInt(num1);
    let num2New = parseInt(num2);

    if (num1New < 10) {
        console.warn(num1New + num2New);
    } else if (num1New >= 10 && num1New <= 20) {
        alert(num1New - num2New);
    } else if (num1New >= 21 && num1New <= 29) {
        alert(num1 * num2New);
    } else if (num1New >= 30) {
        alert(num1New / num2New);
    } else {
        console.warn(`Invalid number.`);
    }
}

let num1 = prompt('Enter first number: ');
let num2 = prompt('Enter second number: ');

checkNumbers(num1, num2);

function checkInfo(name, age) {
    if (!(name == null || name == '') && !(age == null || age == '')) {
        console.log(`User: ${name}, age: ${age}`);

        isLegalAge(age);
    } else {
        console.log(`Are you a time traveler?`);
    }
}

function isLegalAge(age) {
    if (age >= 18){
        alert(`You are of legal age`);
    } else {
        alert(`You are not allowed here.`);
    }
}

let name = prompt('Enter name');
let age = prompt('Enter age');

checkInfo(name, age);

function getInfo(firstName, lastName, email, password, confirmPassword) {
    if (!(firstName == null || firstName == '') && !(lastName == null || lastName == '') && !(email == null || email == '') && !(password == null || password == '') && !(confirmPassword == null || confirmPassword == '') && password.length >= 8 && password == confirmPassword) {
        alert(`Thank you for logging your information!`);
    }
    else {
        alert(`Please fill in your information!`);
    }
}

let firstName = prompt('Enter first name');
let lastName = prompt('Enter last name');
let email = prompt('Enter email address');
let password = prompt('Enter password');
let confirmPassword = prompt('Enter confirmation password');

getInfo(firstName, lastName, email, password, confirmPassword);